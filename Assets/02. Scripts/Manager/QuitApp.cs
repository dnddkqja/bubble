using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 앱 종료
/// </summary>

public class QuitApp : MonoBehaviour
{
    public void OnApplicationQuit()
    {

        Application.Quit();
    }
}
