using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Settings;
using UnityEngine.Localization;

public class Login : MonoBehaviour
{
    [Header("인풋필드, 로그인")]
    [SerializeField]
    TMP_InputField inputID;
    [SerializeField]
    TMP_InputField inputPW;
    [SerializeField]
    Button LoginButton;

    [Header("계정 관련 버튼")]
    [SerializeField]
    Button findID;
    [SerializeField]
    Button findPW;
    [SerializeField]
    Button signUp;
    [SerializeField]
    Button secession;

    

    [Header("로딩 패널")]
    [SerializeField]
    GameObject loadingPanel;

    [Header("로그인 관련 토글")]
    [SerializeField]
    Toggle toggleAutoLogin;
    [SerializeField]
    Toggle toggleSaveID;

    bool autoLogin = false;
    bool saveID = false;

    [Header("앱 종료")]
    [SerializeField]
    Button exit;      

    void Start()
    {
        // 앱 종료 버튼
        //exit.onClick.AddListener(() => {
        //    Locale curLang = LocalizationSettings.SelectedLocale;
        //    string text = LocalizationSettings.StringDatabase.GetLocalizedString("Table 01", "종료", curLang);
        //    PopupManager.Instance.ShowTwoButtnPopup(text, null, ExitApp);
        //});

        // 로그인 버튼
        LoginButton.onClick.AddListener(() => {
            CheckLogin();           
        });

        //if (autoLogin)
        //{
        //    // 웹뷰 이닛 기다림
        //    WebviewManager.Instance.isWebviewCreated += AutoLogin;
        //    //Invoke("AutoLogin", 3f);
        //    //AutoLogin();
        //}
        //else if (Util.LoadData(AESUtil.SaveID).Equals("1"))
        //{
        //    string id = Util.LoadData(AESUtil.USER_ID);
        //    if (!id.Equals(""))
        //    {
        //        inputID.text = id;
        //    }
        //}
#if UNITY_EDITOR_WIN
        
#endif
    }

    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Return))
        {
            CheckLogin();
        }
    }       

    void AutoLogin()
    {
        // 자동 로그인 
        if (Util.LoadData(AESUtil.AutoLogin).Equals("1"))
        {
            // 로그인 시키기
            string id = Util.LoadData(AESUtil.USER_ID);
            string pass = Util.LoadData(AESUtil.USER_PASS);
            ApiLogin(id, pass);
        }
    }

    void NavigateTo(TMP_InputField targetField)
    {
        targetField.Select();
        targetField.ActivateInputField();
    }

    void SetToggleButton()
    {
        SetToggleIsOn(AESUtil.AutoLogin, toggleAutoLogin);
        SetToggleIsOn(AESUtil.SaveID, toggleSaveID);
    }

    void SetToggleIsOn(string key, Toggle toggle)
    {
        string value = "0";
        int savedIntValue = 0;
        if (!Util.LoadData(key).Equals(""))
        {
            value = Util.LoadData(key);
        }
        
        savedIntValue = int.Parse(value);
        if(toggle == toggleAutoLogin)
        {
            autoLogin = savedIntValue == 1 ? true : false; 
        }
        else if (toggle == toggleSaveID)
        {
            saveID = savedIntValue == 1 ? true : false;
        }

        bool boolValue = savedIntValue != 0;
        toggle.isOn = boolValue;
    }

    void ToggleAutoLogin(bool value)
    {
        autoLogin = value;
        SaveBoolean(autoLogin, AESUtil.AutoLogin);
    }

    void ToggleSaveID(bool value)
    {
        saveID = value;
        SaveBoolean(saveID, AESUtil.SaveID);
    }

    void SaveBoolean(bool boolean, string key)
    {
        int intValueToboolean = boolean ? 1 : 0;
        Util.SaveData(key, intValueToboolean.ToString());
    }

    void CheckLogin()
    {
        PhotonManagerLobby.Instance.ConnectToPhoton();
    }

    void ApiLogin(string id , string pass)
    {        
        Dictionary<string, object> requestData = new Dictionary<string, object>();
        requestData.Add("userEmail", id);
        requestData.Add("userPassword", pass);
        string appOs = "";
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_EDITOR_OSX
        appOs = "PC";        
#elif UNITY_ANDROID
        appOs = "AOS";
#elif UNITY_IOS
        appOs = "IOS";
#endif
        requestData.Add("appOs", appOs);        
        
        
    }   

    bool CheckEmail(string email)
    {
        string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
        return Regex.IsMatch(email, pattern);
    }
    bool CheckPass(string password)
    {
        string passwordPattern = @"^(?=.*[A-Z].*[A-Z])(?=.*[a-z].*[a-z])(?=.*\d.*\d)(?=.*[^a-zA-Z\d].*[^a-zA-Z\d]).{10,}$";
        return Regex.IsMatch(password, passwordPattern);


        //// Check for a minimum length of 10 characters
        //if (password.Length < 10)
        //    return false;

        //// Count the occurrences of uppercase, lowercase, numbers, and special characters
        //int uppercaseCount = 0;
        //int lowercaseCount = 0;
        //int numberCount = 0;
        //int specialCharCount = 0;

        //foreach (char c in password)
        //{
        //    if (char.IsUpper(c))
        //        uppercaseCount++;
        //    else if (char.IsLower(c))
        //        lowercaseCount++;
        //    else if (char.IsDigit(c))
        //        numberCount++;
        //    else
        //    {
        //        // Add more special characters if needed
        //        if ("!@#$%^&*()-_=+[]{}|;:,.<>?".Contains(c))
        //            specialCharCount++;
        //    }
        //}

        //// Check if each requirement is met
        //return uppercaseCount >= 2 && lowercaseCount >= 2 && numberCount >= 2 && specialCharCount >= 2;
    }

    void ExitApp()
    {
        Application.Quit();
    }
}
