using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 속성 정리
/// </summary>

public class PlayerAttributes : MonoBehaviour
{
    public bool bubbleAble;
    public bool bubbleAccept;
    public float speed = 3f;

    public PhotonView photonView;
    public string seq;
    public string lang;
    public bool isGuest;

    Rigidbody rb;

    void Awake()
    {
        bubbleAble = false;
        bubbleAccept = true;

        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {    
        photonView = GetComponentInParent<PhotonView>();

        if (photonView == null)
        {
            Debug.Log("포톤뷰 널");
            return;
        }

        

        
    }

    IEnumerator DropPlayer()
    {
        rb.useGravity = true;
        rb.isKinematic = false;
        FreezePositionY(false);
        DebugCustom.Log("낙하 중");
        yield return new WaitForSeconds(3f);

        rb.useGravity = false;
        rb.isKinematic = true;
        FreezePositionY(true);
        DebugCustom.Log("낙하 완료");
    }

    void FreezePositionY(bool freeze)
    {
        // Get the current constraints
        RigidbodyConstraints currentConstraints = rb.constraints;

        // Modify the constraints based on the freeze parameter
        if (freeze)
        {
            // Freeze the X-axis position
            currentConstraints |= RigidbodyConstraints.FreezePositionY;
        }
        else
        {
            // Unfreeze the X-axis position
            currentConstraints &= ~RigidbodyConstraints.FreezePositionY;
        }

        // Apply the modified constraints back to the Rigidbody
        rb.constraints = currentConstraints;
    }
}
